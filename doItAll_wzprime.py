import subprocess
import os
import pdb

os.system("source setup.sh")
os.system("echo 'START WZ prime'")
os.system("./run_wzprime.sh")
os.system("echo 'Now to rewrite the xml files for systematics'")
p4 = subprocess.Popen(["python", "make_xml_wzprime.py"], stdout=subprocess.PIPE)
s4 = p4.communicate()
for line in s4:
 print(line)
#os.system("python make_xml_wzprime.py")
os.system("echo 'Now to create the wzprime roostat workspace from new xmls'")
p5 = subprocess.Popen(["python", "make_workspace_wzprime.py"], stdout=subprocess.PIPE)
s5 = p5.communicate()
for line in s5:
 print(line)
#os.system("python make_workspace_wzprime.py")
os.system("echo 'Now to initiaite limit setting wzprime'")
os.system("./run_workspace_wzprime.sh")
os.system("echo 'Results exists in results/ dir. Now run makeplot scripts.'")
p1 = subprocess.Popen(["python", "makePlots_wzprime_combined.py"], stdout=subprocess.PIPE)
p2 = subprocess.Popen(["python", "makePlots_wzprime_el.py"], stdout=subprocess.PIPE)
p3 = subprocess.Popen(["python", "makePlots_wzprime_mu.py"], stdout=subprocess.PIPE)
strings = ["" for x in range(3)]
strings[0] = p1.communicate()
strings[1] = p2.communicate()
strings[2] = p3.communicate()
for strng in strings:
 for line in strng:
  print(line)
os.system("echo 'DONE with wzprime'")

