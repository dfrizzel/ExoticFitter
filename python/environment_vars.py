import os

def get_dijet_env():
	framework_path = ""
	try:  
		framework_path = os.environ["DJ_FRAMEWORK"]
	except KeyError: 
   		print "Please set the environment variable DJ_FRAMEWORK"
	
	return framework_path
def get_histfitter_env():
	exotic_path = ""
	try:
		exotic_path = os.environ["HISTFITTER"]
	except KeyError:
		print "Please set setup Histfitter Environment."
	return exotic_path
