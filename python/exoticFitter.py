from ROOT import TFile,TMath,TF1,TH1D,TH1F,TCanvas,TGraph,gPad
from math import log,exp
from array import array
from decimal import Decimal
import time
def ff6( x, par ):#Here is a 6 parameter fit function, take the observable and paramter list 
     Ecm=13000.;#as an argument, and returns the value of the function.
     fCenterOfMassEnergy = Ecm;
     fUseWindowExclusion = False;
     x = x / fCenterOfMassEnergy;
     ff1=par[0]*TMath.Power((1.0-x),par[1])
     ff2=TMath.Power(x,(par[2]+par[3]*log(x)+par[4]*log(x)*log(x)+par[5]*log(x)*log(x)*log(x)))
     ff=ff1*ff2;
     return ff;

def ff5(x,par):#5 parameter fit function. Takes x coordinate and paramter list
     Ecm=13000.;#as arguments and returns value of the function
     fCenterOfMassEnergy = Ecm;
     fUseWindowExclusion = False;
     x = x / fCenterOfMassEnergy;
     ff1=par[0]*TMath.Power((1.0-x),par[1])
     ff2=TMath.Power(x,(par[2]+par[3]*log(x)+par[4]*log(x)*log(x)))
     ff=ff1*ff2;
     return ff;



def preProcess(histoFile=None,datahist=None,fitlow=0.0,fithigh =13000.,zprimeMass=500,sigModel="",signalFile=None,function="x",par=None,AcptLog=None,preweights="bins_m",useGaus=False,gausMean=500,gausSigma=75):
	f1 = TFile(histoFile)#Get file the data is in
	f1.ls()#List files contents (debugging purposes)
	f2 = TFile(signalFile)#get file with signal estimate
	hdata = f1.Get(datahist)#Get data histogram
	hdata.Sumw2()
	zprime = f2.Get(sigModel % (str(zprimeMass)))#signal histogram name with a %s in it that is iterable
	weights = []#list of bin weights
	bkg = []#Don't forget to add in a bkgErr 
	sig = []#these few lines initialize lists for the output 
	dat = []#data list
	datErr = []#
	sigErr = []#
	bkgErr = []#
	binCenter = []#Value of center of data histogram bins
	lowEdge = []#Value of lower edge of data histogram bins
	binWidths = []#list of bin widths
	maximum_of_fitfunction = 20000
	func = TF1("func",function,0,maximum_of_fitfunction)#make a fit function as passed to function
	func.SetParameters(Decimal(par[0]),Decimal(par[1]),Decimal(par[2]),Decimal(par[3]),Decimal(par[4]))#set parameters, Decimal gives precision
	bins = f1.Get(preweights)#Get the weights used by the fitting routine, this will be used to normalize everything
	print("PREWEIGHTS = ")
	bins = list(bins)
	print(bins)
	hdata.GetBinLowEdge(3)-hdata.GetBinLowEdge(2)
	totalSig =0.0#total in windows!
	totalBkg=0.0
	totalData=0.0
		
	#Here begins useful code again
	for x in range(0,hdata.GetNbinsX()):#Loop over each bin in data
		center = hdata.GetBinCenter(x)#Get value of center of the bin
		lowEdgeCheck = hdata.GetBinLowEdge(x)#value of loweredge of bin
		width = float(hdata.GetBinLowEdge(x+1)-hdata.GetBinLowEdge(x))#width
		if center < fithigh and center > fitlow:#If center is within the fit range
			lowEdge.append(hdata.GetBinLowEdge(x))#Fill all lists
			binCenter.append(center)
			binWidths.append(width)
			if useGaus:
				sig.append(1000*TMath.Gaus(center,gausMean,gausSigma))
				sigErr.append(zprime.GetBinError(x)/width)#Get The signal Error
				totalSigma=totalSigma+1000*TMath.Gaus(center,gausMean,gausSigma)
			else:
				sig.append(zprime.GetBinContent(x)/width)#I am dividing by width the make bins equal
				sigErr.append(zprime.GetBinError(x)/width)#Get The signal Error
				totalSig = totalSig+zprime.GetBinContent(x)#The totals are not normalized.
			weights.append(bins[x])#weights just to send to output
			FunctionValueAverage =   func.Integral(hdata.GetBinLowEdge(x),hdata.GetBinLowEdge(x+1))#integrate the fit function over the bin width
			#bkg.append(bins[x]*func.Eval((hdata.GetBinLowEdge(x+1)+hdata.GetBinLowEdge(x))/2 )/width)
			bkg.append(bins[x]*FunctionValueAverage/(width*width))#multiply by the bins wieght to unweight from fitting routine, divide by width once for the average value, and divide by width again to normailze to unit bin widths
			dat.append(hdata.GetBinContent(x)/width)#normalize data by bin width
			datErr.append(hdata.GetBinError(x)/width)#Store the error
			totalBkg = totalBkg+bins[x]*FunctionValueAverage/width
			totalData = totalData+hdata.GetBinContent(x)
			#The dynamic bin width requires special care
			#Then a chek is performed to ensure the edge of the bin that is outside the declared signal region is also added
		if  (hdata.GetBinCenter(x+1) > fithigh and hdata.GetBinCenter(x) < fithigh):#The upper edge of bin is over srhigh & center is under 
			lowEdge.append(hdata.GetBinLowEdge(x+1))
			print("ExoticFitter edge Check high ")
		#Here are some debug lines below
		#if(len(binCenter)>0):
		#	print(x)
		#	print("data: "+str(dat[-1])+" lowEdge: "+str(lowEdge[-1])+" Center: "+str(binCenter[-1])+" bkg: "+str(bkg[-1]))
	print("The lowEdge, center, and data,  array lengths")
	print(len(lowEdge))#These print statements are just debug
	print(len(binCenter))
	print(len(dat))
	print(dat)
	print("data is above")
	print(lowEdge)
	print("lowEdge list is above")
	print(binCenter)
	print("binCenter list is above")
	print("Zprime events= " + str(zprime.Integral()))
	print("mass window Z prime= "+str(totalSig))

	logfile = open(AcptLog %(str(zprimeMass)),"w")#The logfile gives signal events, signal in mass window,
	logfile.write(str(totalSig)+"\n")#signal to background ratio in mass window, and the lower and higher window edges
	if useGaus:
		logfile.write("1000")
	else:
		logfile.write(str(zprime.Integral())+"\n")
	logfile.write(str(totalSig/totalBkg)+"\n")
	logfile.write(str(lowEdge[0])+"\n")
	logfile.write(str(lowEdge[-1])+"\n")
	logfile.close()
	for x in range(0,len(dat)):
		dat[x]=dat[x]*binWidths[x]
		bkg[x]=bkg[x]*binWidths[x]
		sig[x]=sig[x]*binWidths[x]
	return binCenter,lowEdge,sig,bkg,dat,sigErr,datErr,binWidths,weights,totalSig,totalBkg,totalData#yeah I know this is too many returns.


def graf(binCenter,sig,bkg,dat,datErr):
	f2 = TFile("output.root","recreate")
	model = TGraph(len(binCenter),array('d',binCenter),array('d',bkg))
	signal = TGraph(len(binCenter),array('d',binCenter),array('d',sig))
	data = TGraph(len(binCenter),array('d',binCenter),array('d',dat))
	model.Write("bkg")
	signal.Write("sig")
	data.Write("dat")
	f2.Write()

#par = [6.64556e-07, 2.26415e+00 , -1.19293e+01, -2.90477e+00 , -4.91382e-01 ,-4.17783e-02]
#print(ff6(x,par))

#void makeHistoFromFitFunction(){
#auto fitFunc = new TF1("fitfunc","x*gaus(0) + [3]*form1",0,10);
#}
