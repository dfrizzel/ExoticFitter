from ROOT import *
import pdb
from decimal import Decimal

def getGoodRatioEdges(signalFile,signalHist,par,function,weightsFile,preweights=None,require1Sigma=False):
 f1 = TFile(signalFile)
 h1 = f1.Get(signalHist)
 tot=h1.Integral()
 mx =1
 fw = TFile(weightsFile)
 weights=fw.Get(preweights)#This is a weight file that must be passed to calcValue to get right model Estimate
#This for loop finds the highest signal bin(start of alg)
 #pdb.set_trace() 
 for x in range(1,h1.GetNbinsX()):
   if h1.GetBinContent(x) > h1.GetBinContent(mx):
     mx = x
 win=h1.GetBinContent(mx)
 winBackground=calcValue(h1.GetBinLowEdge(mx),h1.GetBinLowEdge(mx+1),mx,function,par,weights)
 downStep=1
 upStep=1
 binArray=[]
 binArray.append(mx)
 
 ratio=win/winBackground#sig/back of right bin
 try:
  while True:
    BackgroundBinUp=calcValue(h1.GetBinLowEdge(mx+upStep),h1.GetBinLowEdge(mx+upStep+1),mx+upStep,function,par,weights)
    BackgroundBinDown=calcValue(h1.GetBinLowEdge(mx-downStep),h1.GetBinLowEdge(mx-downStep+1),mx-downStep,function,par,weights)
    ratioUp=h1.GetBinContent(mx+upStep)/BackgroundBinUp#sig/back of right bin
    ratioDown=h1.GetBinContent(mx-downStep)/BackgroundBinDown#sig/back left bin
    if  ratioUp>ratioDown:
      binArray.append(mx+upStep)
      win+=h1.GetBinContent(mx+upStep)
      winBackground+=BackgroundBinUp
      newRatio=win/winBackground 
      upStep+=1
    else :
      binArray.append(mx-downStep)
      win+=h1.GetBinContent(mx-downStep)
      winBackground+=BackgroundBinDown
      newRatio=win/winBackground
      downStep+=1
    if (require1Sigma==True and ratio>newRatio and win/tot>0.68) or (require1Sigma==False and ratio>newRatio):
      binArray=sorted(binArray)
      break
    ratio=newRatio
    binArray=sorted(binArray)
 except:
    print("FAILED TO FIND MASS WINDOW")
 binArray=sorted(binArray)
 windowCheck=0
 print("here starts the answers")
 print(binArray)
 h2 = h1.Clone()
 h2.Reset()
 h2.SetFillColor(kYellow)
 for x in binArray:
 	windowCheck+=h1.GetBinContent(x)
  	print(x)
  	print(windowCheck)
	h2.SetBinContent(x,h1.GetBinContent(x))
 print("tot= "+str(tot))
 print("ratio= "+str(ratio))
 print("left edge = "+str(h1.GetBinLowEdge(binArray[0])))
 print("right edge = "+str(h1.GetBinLowEdge(binArray[-1]+1)))
 print(mx)
 print(h1.GetBinContent(mx))
 c1 = TCanvas()
 #1.GetXaxis().SetRange(100,1000)
 h1.Draw("e")
 h2.Draw("same")
 print("A plot of the signal region chosen will be saved to figs/")
 print("insert c to continue")
 #pdb.set_trace()
 c1.SaveAs("figs/"+signalHist+"_limitWindow.png")
 return h1.GetBinLowEdge(binArray[0]),h1.GetBinLowEdge(binArray[-1]+1)







def getWindowEdges(signalFile,signalHist):
 f1 = TFile(signalFile)
 h1 = f1.Get(signalHist)
 tot=h1.Integral()
 mx =0
 for x in range(0,h1.GetNbinsX()):
   if h1.GetBinContent(x) > h1.GetBinContent(mx):
     mx = x
 win=h1.GetBinContent(mx)
 
 ratio=win/tot
 downStep=1
 upStep=1
 binArray=[]
 binArray.append(mx)
 try:
  #while h1.GetBinContent(binArray[0])>h1.GetBinContent(mx)/2 and h1.GetBinContent(binArray[-1])>h1.GetBinContent(mx)/2 :
  while ratio < .68 :
    if h1.GetBinContent(mx+upStep)>h1.GetBinContent(mx-downStep):
      binArray.append(mx+upStep)
      win+=h1.GetBinContent(mx+upStep)
      upStep+=1
    else :
      binArray.append(mx-downStep)
      win+=h1.GetBinContent(mx-downStep)
      downStep+=1
    ratio=win/tot
    binArray=sorted(binArray)
 except:
    print("FAILED TO FIND MASS WINDOW")
 binArray=sorted(binArray)
 windowCheck=0
 print("here starts the answers")
 print(binArray)
 h2 = h1.Clone()
 h2.Reset()
 h2.SetFillColor(kYellow)
 for x in binArray:
 	windowCheck+=h1.GetBinContent(x)
  	print(x)
  	print(windowCheck)
	h2.SetBinContent(x,h1.GetBinContent(x))
 print("tot= "+str(tot))
 print("ratio= "+str(ratio))
 print("left edge = "+str(h1.GetBinLowEdge(binArray[0])))
 print("right edge = "+str(h1.GetBinLowEdge(binArray[-1]+1)))
 print(mx)
 print(h1.GetBinContent(mx))
 c1 = TCanvas()
 #1.GetXaxis().SetRange(100,1000)
 h1.Draw("e")
 h2.Draw("same")
 print("insert c to continue")
 c1.SaveAs("figs/"+signalHist+"_limitWindow.png")
 return h1.GetBinLowEdge(binArray[0]),h1.GetBinLowEdge(binArray[-1]+1)

def calcValue(binLow,binHigh,binNum,function,par,unweight):
	func = TF1("func",function,0,20000)
	func.SetParameters(Decimal(par[0]),Decimal(par[1]),Decimal(par[2]),Decimal(par[3]),Decimal(par[4]))
	val = unweight[binNum]*func.Integral(binLow,binHigh)/(binHigh-binLow)
	return val

