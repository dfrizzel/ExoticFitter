from ROOT import TF1,TH1D,TFile,TCanvas 
from decimal import Decimal
import pdb

def get_parameters(dataFile,dataName,func,numParam,low,high,weights="bins_m"):
	ff = TFile(dataFile)
	#print(ff)
	h1 = ff.Get(dataName)
	#print(dataName)
	#print(h1)
	bins = ff.Get(weights)
	#print(bins)
	h1.Divide(bins)
	c1 = TCanvas()
	bins.Draw()
	#pdb.set_trace()
	h1.Draw("same")
	f1 = TF1("smooth",func,low,high)
	#h1.Fit("smooth")
	for i in range(20):
		fitr=h1.Fit(f1,"SMR0")
   		print "Status=",int(fitr), " is valid=",fitr.IsValid()
		if (fitr.IsValid()==True): break;
	fitr.Print()
	c1 = TCanvas()
	c1.SetLogy()
	c1.SetLogx()
	h1.Draw()
	f1.Draw("same")
	print "Is valid=",fitr.IsValid()
	print "Histogram Normalization=", h1.Integral()

	par = f1.GetParameters()
	chi2= f1.GetChisquare()
	ndf=f1.GetNDF()
	print "Chi2=", chi2," ndf=",ndf, " chi2/ndf=",chi2/ndf

	params = []
	for x in range(0,numParam):
		params.append(float(par[x]))
	#pdb.set_trace()
	return params

if __name__ == "__main__":
	#This is just a test script to make sure the fits are working
	stuff = get_parameters("/users/chekanov/work/Dijet2016lepton/analysis/out/sys0/data/data.root", \
			"JetJetMass", \
			"([0]*TMath::Power((1.0-x/13000),[1]))*(TMath::Power(x/13000,([2]+[3]*log(x/13000)+[4]*log(x/13000)*log(x/13000))))",\
			5,190,7000)
	print(stuff)
