import os

masses =["250","300","350","400","450","500","1000","1500","2000","2500","3000","4000","5000","6000"]
channels = ["_el","_mu",""]

for chan in channels:
 for mass in masses:
	f = open("config/MyHistoAnalysis_JetJetMass%s_rhoT%s/SPlusB_UserRegion_mass.xml" % (chan,mass) ,"w")

	content = """ <!DOCTYPE Channel SYSTEM '../HistFactorySchema.dtd'> 

<Channel Name="UserRegion_mass"> 

  <Data HistoName="hData_UserRegion_obs_mass" InputFile="data/MyHistoAnalysis_JetJetMass%s_rhoT%s.root" /> 

  <Sample Name="Bkg" HistoName="hBkgNom_UserRegion_obs_mass" InputFile="data/MyHistoAnalysis_JetJetMass%s_rhoT%s.root" NormalizeByTheory="False"> 
    <StatError Activate="True"/> 
  </Sample> 

  <Sample Name="Sig" HistoName="hSigNom_UserRegion_obs_mass" InputFile="data/MyHistoAnalysis_JetJetMass%s_rhoT%s.root" NormalizeByTheory="True"> 
    <StatError Activate="True"/> 
    <OverallSys Name="JES" High="1.03" Low="0.97" /> 
    <OverallSys Name="pdf" High="1.01" Low="0.99" /> 
    <HistoSys Name="JET_EtaIntercalibration_NonClosure" HistoNameHigh="hJET_EtaIntercalibration_NonClosure__1upNom_UserRegion_obs_mass" HistoNameLow="hJET_EtaIntercalibration_NonClosure__1downNom_UserRegion_obs_mass" /> 
    <HistoSys Name="JET_GroupNP_1" HistoNameHigh="hJET_GroupNP_1__1upNom_UserRegion_obs_mass" HistoNameLow="hJET_GroupNP_1__1downNom_UserRegion_obs_mass" /> 
    <HistoSys Name="JET_GroupNP_2" HistoNameHigh="hJET_GroupNP_2__1upNom_UserRegion_obs_mass" HistoNameLow="hJET_GroupNP_2__1downNom_UserRegion_obs_mass" /> 
    <HistoSys Name="JET_GroupNP_3" HistoNameHigh="hJET_GroupNP_3__1upNom_UserRegion_obs_mass" HistoNameLow="hJET_GroupNP_3__1downNom_UserRegion_obs_mass" /> 
    <HistoSys Name="JET_JER_SINGLE_NP" HistoNameHigh="hJET_JER_SINGLE_NP__1upNom_UserRegion_obs_mass" HistoNameLow="hJET_JER_SINGLE_NP__1downNom_UserRegion_obs_mass" /> 
    <HistoSys Name="LEP_ES" HistoNameHigh="hLEP_ES__1upNom_UserRegion_obs_mass" HistoNameLow="hLEP_ES__1downNom_UserRegion_obs_mass" /> 
    <NormFactor Name="mu_Sig" Val="1" High="1" Low="0" Const="False" /> 
  </Sample> 
 
</Channel> """ % (chan,mass,chan,mass,chan,mass)

	f.write(content)
