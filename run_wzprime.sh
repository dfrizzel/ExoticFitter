HistFitter=${HISTFITTER}/scripts/HistFitter.py
echo "Kick the tires and light the fires!"
#for MASS in 600 800 1000 1250 1500 1750 2000 2500 3000 4000 5000 6000

#here is the part where I would set the file names!


#for MASS in 250 300 350 400 450 500 1000 1500 2000 2500 3000 4000 5000 6000 #ranges for wzprimeT
for MASS in 250 500 1000 2000 3000 4000 5000 6000 6500 #ranges for WZ
do
echo "Initiating run for MASS="$MASS
sed -i '/MASSFLAG/c\modelMass='$MASS'#MASSFLAG' analysis/dijetLepton/MyHistoAnalysis_wzprime_combined.py
sed -i '/MASSFLAG/c\modelMass='$MASS'#MASSFLAG' analysis/dijetLepton/MyHistoAnalysis_wzprime_el.py
sed -i '/MASSFLAG/c\modelMass='$MASS'#MASSFLAG' analysis/dijetLepton/MyHistoAnalysis_wzprime_mu.py
#low=$(sqlite3 <<< 'select '$MASS'*0.9;')
#high=$(sqlite3 <<< 'select '$MASS'+0.01*'$MASS';')
#sed -i '/WINDOWLOWFLAG/c\fl='$low'#WINDOWLOWFLAG' analysis/dijetLepton/MyHistoAnalysis.py
#sed -i '/WINDOWHIGHFLAG/c\fh='$high'#WINDOWHIGHFLAG' analysis/dijetLepton/MyHistoAnalysis.py


$HistFitter -t  -w  analysis/dijetLepton/MyHistoAnalysis_wzprime_combined.py
$HistFitter -t  -w  analysis/dijetLepton/MyHistoAnalysis_wzprime_el.py
$HistFitter -t  -w  analysis/dijetLepton/MyHistoAnalysis_wzprime_mu.py
#wait

done
