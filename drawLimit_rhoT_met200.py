

def draw(filename=None):
 
 import sys
 #sys.path.append("/users/chekanov/work/Dijet2016lepton/ana/modules")
 sys.path.append("/users/dfrizzell/utils/modules/")
 import pdb
 from ROOT import TLimit,THStack,TLimitDataSource,TConfidenceLevel
 # import atlas styles
 from AtlasStyle import *
 from AtlasUtils import *
 from initialize  import *
 from global_module import *
 # is it batch?
 # if batch, set input to batch
 myinput="-b"
 if (len(sys.argv) > 1):
    myinput = sys.argv[1]
 print "Mode=",myinput
 
 
 
 # from array import array
 # from functions import *
 
 
 gROOT.Reset()
 figdir="figs/"
 epsfig=figdir+__file__.replace(".py",".eps")
 nameX="M_{jj} [TeV]"
 nameY="#sigma #times #times #it{A} #times BR [pb]"
 Ymin=0.0001 
 Ymax=100.0
 Xmin=0.1
 Xmax=7  
 ISYS=0
 
 
 NN=0
 
 ######################################################
 gROOT.SetStyle("Plain");
 
 # gStyle.SetLabelSize(0.8,"xyz");
 c1=TCanvas("c","BPRE",10,10,600,400);
 c1.Divide(1,1,0.008,0.007);
 ps1 = TPostScript( epsfig,113)
 
 gPad.SetLogy(1)
 gPad.SetLogx(0)
 gPad.SetTopMargin(0.05)
 gPad.SetBottomMargin(0.15)
 gPad.SetLeftMargin(0.12)
 gPad.SetRightMargin(0.04)
 gPad.SetTickx()
 gPad.SetTicky()
 
 h=gPad.DrawFrame(Xmin,Ymin,Xmax,Ymax);
 h.Draw()
 
 
 # get baysian
 ffile=TFile(filename)
 ffile.ls()
 
 # observed limit
 observed=ffile.Get("Observed")
 rescaleaxis(observed,0.001)
 
 observed.SetMarkerColor(1)
 observed.SetMarkerSize(1)
 observed.SetMarkerStyle(20)
 observed.SetLineColor(1)
 observed.SetLineWidth(3)
 observed.SetLineStyle(1)
 observed.SetFillColor(0)
 print "Observed limit:"
 observed.Print("all")
 
 
 
 expected1sigma=ffile.Get("oneSigmaBand")
 rescaleaxis(expected1sigma,0.001)
 
 expected1sigma.SetFillColor(oneSigmaBandColour)
 expected1sigma.SetLineColor(1)
 expected1sigma.SetLineWidth(2)
 expected1sigma.SetLineStyle(2)
 
 print "Expected 1 sigma limit:"
 expected1sigma.Print("all")
 
 expected2sigma=ffile.Get("twoSigmaBand")
 rescaleaxis(expected2sigma,0.001)
 
 expected2sigma.SetFillColor(twoSigmaBandColour)
 expected2sigma.SetLineColor(1)
 expected2sigma.SetLineWidth(2)
 expected2sigma.SetLineStyle(2)
 
 print "Expected 2 sigma limit:"
 expected2sigma.Print("all")
 
 
 expected2sigma.Draw("3") # 2-sigma expectation error bands
 expected1sigma.Draw("L3") # 1-sigma expectation error bands
 expected1sigma.Draw("LX") # Center of expectation
 #observed.Draw("L")
 #observed.Draw("P")
 
 ## cross section
 cross1=TGraphErrors()
 cross1.SetLineColor( ROOT.kRed )
 cross1.SetMarkerColor(4)
 cross1.SetMarkerSize(1)
 cross1.SetMarkerStyle(24)
 cross1.SetLineWidth(3)
 cross1.SetLineStyle(2)
 kk=0
 for line in open( "/users/dfrizzell/DijetLepton2017/ana/root/dijets_sig_rhoT_met200.d"):
         line=line.strip()
         if (len(line)<2): continue
         if (line.startswith("#")): continue
         data = line.split()
         cross1.SetPoint(kk,float(data[0])/1000.,float(data[1]))
         cross1.SetPointError(kk,0,0)
         print kk, " mass=",data[0]," cross=",data[1]
         kk=kk+1
 cross1.Draw("same")
 cross1.Print("all") 

 leg2=TLegend(0.57, 0.6, 0.9, 0.92)
 leg2.SetBorderSize(0)
 leg2.SetTextFont(62)
 leg2.SetFillColor(0)
 leg2.SetTextSize(0.04);
 # leg2.SetHeader("#sqrt{s}=13 TeV Data")
 leg2.AddEntry(observed,"Observed 95% CL","lp")
 leg2.AddEntry(expected1sigma,"Expected 95% CL","l")
 leg2.AddEntry(expected1sigma,"#pm 1 #sigma","fl")
 leg2.AddEntry(expected2sigma,"#pm 2 #sigma","fl")
 leg2.AddEntry(cross1,"#rho_{T}","l")
 leg2.Draw("same");
 
 # myText(0.2,0.81,4,0.05,"Single b-tag")
 myText(0.2,0.73,1,0.04,'#int L dt =  32.7 fb^{-1}')
 myText(0.2,0.66,1,0.04,"pp #sqrt{s}=13 TeV")
 
 ATLASLabel(0.2,0.89,0.10,0.05)
 
 myText(0.7,0.52,4,0.05,"#rho_T #rightarrow W #pi_T ")
 myText(0.7,0.46,4,0.05,"Dijets + #mu / e^{#pm} ")
 
 
 ax=h.GetXaxis(); ax.SetTitleOffset(0.8)
 ax.SetTitle( nameX );
 ay=h.GetYaxis(); ay.SetTitleOffset(0.8)
 ay.SetTitle( nameY );
 ay.SetTitleSize(0.05)
 ax.SetTitleOffset(1.0); ay.SetTitleOffset(1.2)
 ax.SetTitleSize(0.05)
 ax.Draw("same")
 ay.Draw("same")
 
 
 #gPad.RedrawAxis()
 c1.Update()
 ps1.Close()
 c1.SaveAs("figs/publicationPlot_rhoT_met200_combined.pdf")
 print("Enter c to continue")
 #pdb.set_trace()
 if (myinput != "-b"):
               if (raw_input("Press any key to exit") != "-9999"): 
                          c1.Close(); sys.exit(1);
  
