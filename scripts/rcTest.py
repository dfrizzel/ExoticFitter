
import os
rootcorebin = os.getenv("ROOTCOREBIN")
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT import gROOT,gSystem,gDirectory,RooAbsData,RooRandom,RooWorkspace
rcLibName = gSystem.GetFromPipe("ls -d " + rootcorebin + "/obj/*/HistFitter/lib/*HistFitter.so"  )
gROOT.LoadMacro( rcLibName.Data() )

