import subprocess
import os
import pdb

os.system("echo 'This will begin the RhoT Limit process'")
os.system("echo START")
os.system("source setup.sh")
os.system("./run_rho.sh")#This will create the histograms
#pdb.set_trace()
os.system("echo 'Thehistograms are created, now to begin altering the xml files to inlcude systematics...'")

p4 = subprocess.Popen(["python", "make_xml_rhoT.py"], stdout=subprocess.PIPE)
string_p4 = p4.communicate()
for line in string_p4:
 print(line)
#os.system("python make_xml_rhoT.py")#this will change the xml files to incorporate the systematics
#pdb.set_trace()
os.system("echo 'Now to begin creating ROOSTAT workspace from xml files...'")

p5 = subprocess.Popen(["python", "make_workspace_rho.py"], stdout=subprocess.PIPE)
string_p5 = p5.communicate()
for line in string_p5:
 print(line)
#os.system("python make_workspace_rho.py")#this will create the roostats workspace
#pdb.set_trace()
os.system("echo 'Now to begin setting the limits from the roostats workspace'")
os.system("./run_workspace_rho.sh")#this initiates the run. It will throw some warnings but this is fine because we are runing the roostats space
#pdb.set_trace()
os.system("echo 'Results exist in results/ directory. Now lets run the makeplots scripts...'")
os.system("echo 'I have to change python setups to make the plots.... This may break'")
#os.system("python makePlots_rhoT_combined.py")
p1 = subprocess.Popen(["python", "makePlots_rhoT_combined.py"], stdout=subprocess.PIPE)
p2 = subprocess.Popen(["python", "makePlots_rhoT_el.py"], stdout=subprocess.PIPE)
p3 = subprocess.Popen(["python", "makePlots_rhoT_mu.py"], stdout=subprocess.PIPE)
strings = ["" for x in range(3)]
strings[0] = p1.communicate()
strings[1] = p2.communicate()
strings[2] = p3.communicate()
for strng in strings:
 for line in strng:
  print(line)

#pdb.set_trace()
#os.system("python makePlots_rhoT_el.py")
#pdb.set_trace()
#os.system("python makePlots_rhoT_mu.py")
os.system("echo 'DONE with rho'")

