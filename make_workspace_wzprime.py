import os

masses =["250","500","1000","2000","3000","4000","5000","6000","6500"]
channels = ["_el","_mu",""]
filename = "config/MyHistoAnalysis_JetJetMass%s_wzprime%s/SPlusB.xml"

for chan in channels:
 for mass in masses:
	os.system( "hist2workspace " + ( filename % (chan,mass) ) )
