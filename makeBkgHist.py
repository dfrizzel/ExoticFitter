from ROOT import *
import pdb

#SETUP for Electron
modelMass=2000
sfile="/users/dfrizzell/DijetLepton2017/ana/root/dijets_sig_rhoT_el.root"
signalHist="mjj_rhoT_%s_Nominal" % modelMass
fitFunction="([0]*TMath::Power((1.0-x/13000),[1]))*(TMath::Power(x/13000,([2]+[3]*log(x/13000)+[4]*log(x/13000)*log(x/13000))))"
parameters=[0.213939,13.381,-0.681781,0.883043,0.121242]#electron
dataFile="/users/dfrizzell/DijetLepton2017/analysis/out/data.root"
dataHist="JetJetMass_el"
weights="bins_m"

sf = TFile(sfile)
df = TFile(dataFile)
#sf.ls()
hbkg = sf.Get(signalHist)
hd = df.Get(dataHist)
#hd.Sumw2()
hw = df.Get(weights)
hbkg.Reset()
f1 = TF1("fit",fitFunction,0,12000)
i=0
for par in parameters:
 f1.SetParameter(i,par)
 i=i+1
for x in range(0,hd.GetNbinsX()):
 temp = hd.GetBinError(x)
 if(hw.GetBinContent(x)==0.0):
  continue 
 low = hbkg.GetBinLowEdge(x)
 high = hbkg.GetBinLowEdge(x+1)
 hbkg.SetBinContent(x,f1.Integral(low,high)/(high-low)*hw.GetBinContent(x))
 #hd.SetBinContent(x,hd.GetBinContent(x)/hw.GetBinContent(x))
 #hd.SetBinError(x,temp/hw.GetBinContent(x))

hd.Draw()
hbkg.Draw("same")
f1.Draw("same")
bkgModels = TFile("bkgModels.root","recreate")
hbkg.Write("JetJetMass_el_Model")
pdb.set_trace()
