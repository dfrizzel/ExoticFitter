import os

masses = ["250","300","350","400","450","500","1000","1500","2000","2500","3000","4000","5000","6000"]
#channels = ["_el","_mu",""]
channels = ["",]
filename = "config/MyHistoAnalysis_JetJetMass%s_rhoT%s_met100/SPlusB.xml"

for chan in channels:
 for mass in masses:
	os.system( "hist2workspace " + ( filename % (chan,mass) ) )
