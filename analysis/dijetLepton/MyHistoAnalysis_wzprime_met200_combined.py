import pdb
"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Minimal example configuration with two different uncertainties            * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
import pdb
import os

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

#import exoticFitter
#import exoticFitter as ef
#import exoticFitter_v2 as ef
import exoticFitter as ef
import window as win
#The above two modules are exoticFitterMods
##########################
# Set observed and expected number of events in counting experiment
data     = [] 	# Number of events observed in data
bkg      = [] 	# Number of predicted bkg events
sig      = [] 	# Number of predicted signal events
bkgErr   = [] 	# (Absolute) Statistical error on bkg estimate
sigErr   = [] 	# (Absolute) Statistical error on signal estimate
#Allthe above lists will get changed by preprocess
#Change Name for the Shape
#Be sure and change the analysis NAME and CHANNEL throughout the code!
lumiError = 0.045 	# Relative luminosity uncertainty



###################################
#######ExoticFitter Parameters#####


import environment_vars as env
framework_dir = env.get_dijet_env()
work_dir = env.get_histfitter_env()
print("Dijet framework directory is : " + framework_dir)
print("Working Directory is : " + work_dir)

#scanVal=50#not used Only for the Gaussian limits option. Not implemented 
modelMass=6500#MASSFLAG
func="([0]*TMath::Power((1.0-x/13000),[1]))*(TMath::Power(x/13000,([2]+[3]*log(x/13000)+[4]*log(x/13000)*log(x/13000))))"
#par5=[0.193804,13.8641,-0.981434,0.972174,0.153997]#FLAGmuon
#par5=[0.212911,13.3731,-0.687061,0.884446,0.121341]#FLAGelectron
#par5=[0.0658711,11.6606,-2.13728,0.618434,0.113516]#FLAGcombined
#-------------New data Parameters----------
#par5=[0.0384846,10.9898,-2.50951,0.528512,0.106427]#CombinedNew
#par5=[0.0711335,12.6108,-1.67744,0.804152,0.140406]#MuonNew
#par5=[0.213939,13.381,-0.681781,0.883043,0.121242]#ElectronNew
#-----------------Met Cut Parameters-------
#par5=[0.00289639,9.21955,-4.13134,-0.119095,0.0348037]#combinedMetCut
par5=[0.00016838,6.34012,-6.77265,-0.968447,-0.0463547]#combinedMetCut
#Above you will want to change paramters for fit
weightsName="bins_m"#This is a Weighting factor previously used in fitting
AcptLg=work_dir + "/results/wzprime_met200_acceptance_%s.log"#the %s will be model mass. This is just a output file that stores # events in window,etc
modelName="mjj_WZprime_%s_Nominal"#The histogram with the model binned same as data, %s is modelMass
sysBase="mjj_WZprime_%s_"
#sfile="/users/dfrizzell/signal_samples/dijets_sig_rhoT.root"#File that contains signal histogram
sfile=framework_dir + "/ana/root/dijets_sig_wzprime_met200.root"
#dataFile="~/Dijet2016lepton/analysis/out/sys1/data/data.root"
#dataFile="/users/chekanov/work/Dijet2016lepton/analysis/out/sys0/data/data.root"
dataFile=framework_dir + "/analysis/out/sys0/data/data.root"
dataName="JetJetMass_ptmiss100"#data histogram
analysisNAME = "MyHistoAnalysis_JetJetMass_wzprime%s_met200"#Unique analysis name. the fit output will be stored in results/analysisNAME



configMgr.useCacheToTreeFallback = True # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile = "data/" + analysisNAME % (modelMass) + ".root" 
configMgr.blindSR = True


#####################################
#####################################



#The next lines is where I retrieve basically all information from the analysis most variables are arrays
#fl,fh = win.getGoodRatioEdges(sfile,modelName % (modelMass),\
#        par5,func,dataFile,preweights=weightsName,require1Sigma=False)
fl = 190
fh = 6000
print((modelName % modelMass))
#pdb.set_trace()
binCenter,lowEdge,sig,bkg,data,sigErr,dataErr,binWidths,weights,totalSig,totalBkg,totalData = \
        ef.preProcess(histoFile=dataFile, \
        datahist=dataName, \
        fitlow=fl, \
        fithigh=fh, \
        zprimeMass=modelMass, \
        sigModel=modelName, \
        signalFile=sfile, \
        function=func, \
        par=par5, \
        AcptLog=AcptLg, \
        preweights=weightsName)

#These lists will carry the histogram numbers for the sys errors
Sys_Up = []
Sys_Down = []
Sys_Stat_Up = []
Sys_Stat_Down = []
Sys_Total_Up = []
Sys_Total_Down = []

sysTuple = ("JET_GroupNP_1__","JET_GroupNP_2__","JET_GroupNP_3__","JET_EtaIntercalibration_NonClosure__","JET_JER_SINGLE_NP__","LEP_ES__")
variation_names =("1up","1down")

#This loop will fill the systematic lists so that they may be made into histograms
for sys in sysTuple:
 sys_name_up = sysBase + sys + variation_names[0]  
 print(sys_name_up)
 d1,d2,Sys_Up_temp,d3,d4,statErr_Up_Temp,d5,bw,w,total_sys_Up,tb,td = \
        ef.preProcess(histoFile=dataFile, \
        datahist=dataName, \
        fitlow=fl, \
        fithigh=fh, \
        zprimeMass=modelMass, \
        sigModel=sys_name_up, \
        signalFile=sfile, \
        function=func, \
        par=par5, \
        AcptLog="sysLogs/wzprime_%s_sysUpLog", \
        preweights=weightsName)
 
 Sys_Up.append(Sys_Up_temp)
 Sys_Stat_Up.append(statErr_Up_Temp)
 Sys_Total_Up.append(total_sys_Up)

 sys_name_down = sysBase + sys + variation_names[1] 
 d1,d2,Sys_Down_temp,d3,d4,statErr_Down_Temp,d5,bw,w,total_sys_Down,tb,td = \
        ef.preProcess(histoFile=dataFile, \
        datahist=dataName, \
        fitlow=fl, \
        fithigh=fh, \
        zprimeMass=modelMass, \
        sigModel=sys_name_down, \
        signalFile=sfile, \
        function=func, \
        par=par5, \
        AcptLog="sysLogs/wzprime_%s_sysUpLog", \
        preweights=weightsName)
 
 Sys_Down.append(Sys_Down_temp)
 Sys_Stat_Down.append(statErr_Down_Temp)
 Sys_Total_Down.append(total_sys_Down)


#binLow = lowEdge[0]
binLow = 0

for x in range(0,len(Sys_Up)):
 dumSample = Sample(sysTuple[x]+variation_names[0] ,kBlue)
 dumSample.buildHisto(Sys_Up[x],"UserRegion","mass",binLow,1)
 dumSample.buildStatErrors(Sys_Stat_Up[x],"UserRegion","mass")
 
 dumSample = Sample(sysTuple[x]+variation_names[1] ,kBlue)
 dumSample.buildHisto(Sys_Down[x],"UserRegion","mass",binLow,1)
 dumSample.buildStatErrors(Sys_Stat_Down[x],"UserRegion","mass")



# Set uncorrelated systematics for bkg and signal (1 +- relative uncertainties)
jes = Systematic("JES", configMgr.weights, 1.03,0.97, "user","userOverallSys")
#ucs = Systematic("ModelUncert", configMgr.weights, 1.1,0.9, "user","userOverallSys")

# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1.1,1.2,1.1],[0.9,0.8,0.9], "user","userHistoSys")
#cors = Systematic("cor",configMgr.weights, [1.15,1.25,1.15],[0.85,0.75,0.85], "user","userHistoSys")
pdf = Systematic("pdf",configMgr.weights,1.01,0.99,"user","userOverallSys")
##########################
DummySys = Systematic("KtScaleTop",configMgr.weights,configMgr.weights,configMgr.weights,"weight","histoSys")
# Setting the parameters of the hypothesis test
configMgr.doExclusion=True # True=exclusion, False=discovery
configMgr.nTOYs=5000
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.

configMgr.writeXML = True

##########################

# Give the analysis a name
configMgr.analysisName = analysisNAME % (str(modelMass))
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Define cuts
configMgr.cutsDict["UserRegion"] = "1."
#configMgr.cutsDict["MassWindow"] = "1."
# Define weights
configMgr.weights = "1."
#configMgr.dynamicBins=True
# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
bkgSample.buildHisto(bkg,"UserRegion","mass",binLow,1)
#bkgSample.buildHisto([totalBkg],"MassWindow","cuts")
#bkgSample.buildHisto(bkg,"UserRegion","mass",dynamicWidth=True,lowerEdgeArray=lowEdge)
#bkgSample.buildStatErrors(bkgErr,"UserRegion","mass")
#bkgSample.addSystematic(corb)
#bkgSample.addSystematic(jes)
#bkgSample.addSystematic(pdf)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_Sig",1.,0.0,1000000000.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto(sig,"UserRegion","mass",binLow,1)
#sigSample.buildHisto([totalSig],"MassWindow","cuts")
#sigSample.buildHisto([1000./binWidths[0] ],"UserRegion","mass",binLow,1)
#sigSample.buildHisto([1000./binWidths[0]],"UserRegion","mass",binLow,1)
#sigSample.buildHisto(sig,"UserRegion","mass",dynamicWidth=True,lowerEdgeArray=lowEdge)
sigSample.buildStatErrors(sigErr,"UserRegion","mass")
#sigSample.buildStatErrors(sigErr,"MassWindow","cuts")
#sigSample.addSystematic(ucs)
sigSample.addSystematic(jes)
sigSample.addSystematic(pdf)
#sigSample.addSystematic(DummySys)


dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto(data,"UserRegion","mass",binLow,1)
#dataSample.buildHisto([totalData],"MassWindow","cuts")
#dataSample.buildHisto(data,"UserRegion","mass",dynamicWidth=True,lowerEdgeArray=lowEdge)
dataSample.buildStatErrors(dataErr,"UserRegion","mass")
# Define top-level
ana = configMgr.addFitConfig("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
#ana.addSamples([bkgSample,sigSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_Sig")
#meas.addParamSetting("Lumi",True,1)

# Add the channel
#I have two cahnnels I can turn on or off to do shape or cut and count type fits.
#I think the cut and count is preffered. 
print("Number of bins in channel= "+str(len(binCenter)))
chan = ana.addChannel("mass",["UserRegion"],len(binCenter),binLow,1)#the last two arguements don't actually do anything
#chan = ana.addChannel("cuts",["MassWindow"],1,0.5,1.5)
ana.setSignalChannels([chan])
# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
