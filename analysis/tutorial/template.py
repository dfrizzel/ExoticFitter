
################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic

## First define HistFactory attributes
configMgr.analysisName = "MyDijetAnalysis"
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

## Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1.  # Luminosity of input TTree after weighting
configMgr.outputLumi = 1. # Luminosity required for output histograms
configMgr.setLumiUnits("pb-1")

## setting the parameters of the hypothesis test
#configMgr.nTOYs=5000
configMgr.fixSigXSec=True  # fix SigXSec: 0, +/-1sigma 
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.

## set scan range for the upper limit
#configMgr.scanRange = (0., 1.)

## Suffix of nominal tree
configMgr.nomName = "_NoSys"

## Map regions to cut strings
configMgr.cutsDict = {"SR":"1.0"}

## Systematics to be applied
#jes = Systematic("JES","_JESup","_JESdown","overallSys")
jes = Systematic("JES",None,1.2,0.8,"user","overallSys")

## List of samples and their plotting colours
qcdSample = Sample("QCD",kGreen)
#allbkgSample.setNormFactor("mu_AllBkg",1.,0.,5.)
#allbkgSample.addSystematic(jes)
dataSample = Sample("Data",kBlack)
dataSample.setData()

commonSamples = [qcdSample,dataSample]
configMgr.plotColours = [kGreen,kBlack]

## Parameters of the Measurement
measName = "BasicMeasurement"
measLumi = 1.
measLumiError = 0.039

## Parameters of Channels
cutsRegions = ["SR"]
cutsNBins = 1
cutsBinLow = 0.0
cutsBinHigh = 1.0

## Bkg-only fit
bkgOnly = configMgr.addFitConfig("SimpleChannel_BkgOnly")
bkgOnly.statErrThreshold=None #0.5
bkgOnly.addSamples(commonSamples)
bkgOnly.addSystematic(jes)
#meas = bkgOnly.addMeasurement(measName,measLumi,measLumiError)
#meas.addPOI("mu_SIG")
cutsChannel = bkgOnly.addChannel("cuts",cutsRegions,cutsNBins,cutsBinLow,cutsBinHigh)

## Discovery fit
#discovery = configMgr.adFitConfigClone(bkgOnly,"SimpleChannel_Discovery")
#discovery.clearSystematics()
#sigSample = Sample("discoveryMode",kBlue)
#sigSample.setNormFactor("mu_SIG",1.0, 0.0, 5.0)
#sigSample.setNormByTheory()
#discovery.addSamples(sigSample)
#discovery.setSignalSample(sigSample)

## Exclusion fits (MSUGRA grid)
sigSamples=["Zprime1p5","Zprime2","Zprime2p5","Zprime3","Zprime4","Zprime5"]

#if not 'sigSamples' in dir():
#    sigSamples=["Zprime1p5"]


for sig in sigSamples:
    myTopLvl = configMgr.addFitConfigClone(bkgOnly,"SimpleChannel_%s"%sig)
#    myTopLvl.addSystematic(jes)
    sigSample = Sample(sig,kBlue)
    sigSample.setNormFactor("mu_SIG", 1.0, 0., 5.0)
    sigXSSyst = Systematic("SigXSec",configMgr.weights,1.1,0.9,"user","overallSys")
    sigSample.addSystematic(sigXSSyst)
    #sigSample.addSystematic(jesSig)
    sigSample.setNormByTheory()
    myTopLvl.addSamples(sigSample)
    myTopLvl.setSignalSample(sigSample)
    ch = myTopLvl.getChannel("cuts",cutsRegions)
    myTopLvl.addSignalChannels(ch)
