"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Minimal example configuration with two different uncertainties            * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt

import os

# Setup for ATLAS plotting
from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

#import exoticFitter
import exoticFitter as ef
##########################
# Set observed and expected number of events in counting experiment
data     = [ 7. , 2. , 1.] 	# Number of events observed in data
bkg      = [ 5.	, 3. , 3.] 	# Number of predicted bkg events
sig      = [ 50. , 2. , 3.] 	# Number of predicted signal events
bkgErr   = [ 1. , 1. , 1.] 	# (Absolute) Statistical error on bkg estimate
sigErr   = [ 1. , 1. , 1.] 	# (Absolute) Statistical error on signal estimate
lumiError = 0.0001 	# Relative luminosity uncertainty

binCenter,lowEdge,sig,bkg,data,dataErr = ef.preProcess("~/Dijet2016lepton/analysis/out/sys1/data/data.root ","JetJetMass_mu",980.,1000.)
for x in range(1,len(binCenter)):
	print("binwidth")
	print(binCenter[x]-binCenter[x-1])

# Set uncorrelated systematics for bkg and signal (1 +- relative uncertainties)
#ucb = Systematic("ucb", configMgr.weights, 1.2,0.8, "user","userOverallSys")
#ucs = Systematic("ucs", configMgr.weights, 1.1,0.9, "user","userOverallSys")

# correlated systematic between background and signal (1 +- relative uncertainties)
#corb = Systematic("cor",configMgr.weights, [1.1,1.2,1.1],[0.9,0.8,0.9], "user","userHistoSys")
#cors = Systematic("cor",configMgr.weights, [1.15,1.25,1.15],[0.85,0.75,0.85], "user","userHistoSys")

##########################

# Setting the parameters of the hypothesis test
configMgr.doExclusion=True # True=exclusion, False=discovery
configMgr.nTOYs=5000
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.

configMgr.writeXML = True

##########################

# Give the analysis a name
configMgr.analysisName = "MySingleBinHistoAnalysis"
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

# Define cuts
configMgr.cutsDict["UserRegion"] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(True)
#bkgSample.buildHisto(bkg,"UserRegion","cuts",binLow,binWidth)
bkgSample.buildHisto(bkg,"UserRegion","cuts",dynamicWidth=True,lowerEdgeArray=lowEdge)
#bkgSample.buildStatErrors(bkgErr,"UserRegion","cuts")
#bkgSample.addSystematic(corb)
#bkgSample.addSystematic(ucb)

sigSample = Sample("Sig",kPink)
sigSample.setNormFactor("mu_Sig",1.,0.,10000.)
sigSample.setStatConfig(True)
sigSample.setNormByTheory()
sigSample.buildHisto([1000.],"UserRegion","cuts",dynamicWidth=True,lowerEdgeArray=lowEdge)
#sigSample.buildHisto(sig,"UserRegion","cuts",binLow,binWidth)
#sigSample.buildHisto(sig,"UserRegion","cuts",dynamicWidth=True,lowerEdgeArray=lowEdge)
#sigSample.buildStatErrors(sigErr,"UserRegion","cuts")
#sigSample.addSystematic(cors)
#sigSample.addSystematic(ucs)

dataSample = Sample("Data",kBlack)
dataSample.setData()
#dataSample.buildHisto(data,"UserRegion","cuts",binLow,binWidth)
dataSample.buildHisto(data,"UserRegion","cuts",dynamicWidth=True,lowerEdgeArray=lowEdge)
dataSample.buildStatErrors(dataErr,"UserRegion","cuts")
# Define top-level
ana = configMgr.addFitConfig("SPlusB")
ana.addSamples([bkgSample,sigSample,dataSample])
ana.setSignalSample(sigSample)

# Define measurement
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=lumiError)
meas.addPOI("mu_Sig")
#meas.addParamSetting("Lumi",True,1)

# Add the channel
chan = ana.addChannel("cuts",["UserRegion"],len(binCenter),lowEdge[0],"This Option Does Nothing")#two bins?
ana.setSignalChannels([chan])

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
