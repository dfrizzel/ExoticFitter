from ROOT import *
from drawLimit_wzprime import draw

#before each use be sure that the input root file and the accpentace logs are set corectly!
#import numpy as n

###################################################
#############Edit for EACH RUN######################
#First is for wzprime
#masses =["250","300","350","400","450","500","1000","1500","2000","2500","3000","4000","5000","6000"]
#second is for wzprime
masses =["250","500","1000","2000","3000","4000","5000","6000"]
resultsFile="/users/dfrizzell/ExoticFitter/results/MyHistoAnalysis_JetJetMass_wzprime%s_Output_upperlimit.root"
XsLog="results/wzprime_acceptance_%s.log"
outfilename="limitPlots_wzprime.root"
dataLum=32.7
##############END EDIT###############################

massesFloat = []
zeros = []
for each in masses:
	massesFloat.append(float(each))
	zeros.append(0.)
final = []
limits = []
xAxis = []
exp = []
exp_1 = []
exp_2 = []
exp_m1 = []
exp_m2 = []
eyl1 = []
eyh1 = []
eyl2 = []
eyh2 = []
from array import array

for x in range(0,len(masses)):
 f1 = TFile(resultsFile % (masses[x]))
 mu = f1.Get("hypo_Sig")
 limits.append(mu.UpperLimit())
 #print(mu.())
 afile = open(XsLog % (masses[x]),"r")
 window = float(afile.readline())
 total = float(afile.readline())
 Xs = limits[x]*total/dataLum
 final.append(Xs/1000)#put in pb
 print(final[-1])
 exp.append((mu.GetExpectedUpperLimit()*total)/(dataLum*1000) )
 exp_1.append((mu.GetExpectedUpperLimit(1))*total/(dataLum*1000))
 exp_2.append((mu.GetExpectedUpperLimit(2))*total/(dataLum*1000))
 exp_m1.append((mu.GetExpectedUpperLimit(-1))*total/(dataLum*1000))
 exp_m2.append((mu.GetExpectedUpperLimit(-2))*total/(dataLum*1000))
 xAxis.append(float(masses[x]))
 eyl1.append(exp[x]-exp_m1[x])
 eyl2.append(exp[x]-exp_m2[x])
 eyh1.append(exp_1[x]-exp[x])
 eyh2.append(exp_2[x]-exp[x])

sigma1= TGraphAsymmErrors(len(exp),array('d',massesFloat),array('d',exp),array('d',zeros),array('d',zeros),array('d',eyl1),array('d',eyh1))
sigma2= TGraphAsymmErrors(len(exp),array('d',massesFloat),array('d',exp),array('d',zeros),array('d',zeros),array('d',eyl2),array('d',eyh2))


out = TFile(outfilename,"recreate")
g = TGraphErrors(len(final),array('d',xAxis),array('d',final))
f = TGraphErrors(len(final),array('d',xAxis),array('d',exp))
s1 = TGraph(len(final),array('d',xAxis),array('d',exp_1))
s2 = TGraph(len(final),array('d',xAxis),array('d',exp_2))
d1 = TGraph(len(final),array('d',xAxis),array('d',exp_m1))
d2 = TGraph(len(final),array('d',xAxis),array('d',exp_m2))
g.Write("Observed")
f.Write("Expected")
s1.Write("sigmaUp")
s2.Write("twoSigmaUp")
d1.Write("sigmaDown")
d2.Write("twoSigmaDown")
sigma1.Write("oneSigmaBand")
sigma2.Write("twoSigmaBand")

out.Close()
draw(outfilename)
 

